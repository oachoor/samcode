#!/usr/bin/env php
<?php

/**
 * File containing the script to cleanup old entries from table
 * @param -n
 * @param --table=<table>
 * @param --column=<column>
 * @param --keep=<seconds>
 */
require_once 'autoload.php';

$db     = eZDB::instance();
$cli    = eZCLI::instance();
$logg   = 'Cleanup_table_'. date( 'd_m_Y' ) .'.log';
$script = eZScript::instance( array( 'description' => 'Remove old entries according to keep param value',
                                                      'use-session' => false,
                                                      'use-modules' => true,
                                                      'use-extensions' => true ) );
$script->startup();
$options = $script->getOptions( '[table:][column:][keep:][n]', '', array( 'table' => 'Table',
                                                                          'column' => 'Column values based',
                                                                          'keep' => 'Set keep value in seconds',
                                                                          'n' => 'Do not wait' ) );
$script->initialize();

if ( !isset( $options['n'] ) )
{
    $cli->warning( "This cleanup script is going to remove archived versions according to the settings" );
    $cli->warning( "content.ini/[VersionManagement]/DefaultVersionHistoryLimit and content.ini/[VersionManagement]/VersionHistoryClass" );
    $cli->warning();
    $cli->warning( "You have 5 seconds to break the script (press Ctrl-C)" );
    sleep( 5 );
    $cli->output();
}

/**
 * Check Whether table, column params exists
 */
if ( isset( $options['table'] ) )
{
    $isTable = $db->arrayQuery( "SHOW TABLES LIKE '{$options['table']}'" );

    if ( count( $isTable ) == 1 )
    {
        // Check if column exists
        if ( isset( $options['column'] ) )
        {
            $hasColumn = $db->arrayQuery( "SHOW COLUMNS FROM {$options['table']} LIKE '{$options['column']}'" );

            if ( count( $hasColumn ) == 1 )
            {
                // Keeping some entries...
                if ( isset( $options['keep'] ) )
                {
                    try
                    {
                        $db->begin();
                        $cleanup = $db->query( "DELETE FROM {$options['table']} WHERE UNIX_TIMESTAMP({$options['column']}) < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL {$options['keep']} SECOND))" );
                        $db->commit();
                        $db->close();


                        eZLog::write( "[OK] Table : {$options['table']}", $logg );
                        eZLog::write( "[OK] Column : {$options['column']}", $logg );
                        eZLog::write( "[OK] Cleanup : Keep entries from ".strftime( "%d %b %Y %H:%M:%S", strtotime( "- {$options['keep']} seconds" ) ), $logg );
                    }
                    catch( Exception $e )
                    {
                        $e->getMessage();
                    }
                }
                else
                    eZLog::write( "[ERROR] : Missing keep parameter !", $logg );
            }
            else
                eZLog::write( "[ERROR] : Column {$options['column']} doesn't exists in {$options['table']} table !", $logg );
        }
    }
    else
        eZLog::write( "[ERROR] : Table {$options['table']} doesn't exists !", $logg );
}

$cli->output( $cli->stylize( 'gray', "\n-----------------------------" ), false );
$cli->output( $cli->stylize( 'magenta', "\nSeconds Helper" ), false );
$cli->output( $cli->stylize( 'gray', "              |" ), false );
$cli->output( $cli->stylize( 'gray', "
----------------------------+
1 Day       = 86400 Sec     |
1 Week      = 604800 Sec    |
1 Month     = 2592000 Sec   |
2 Months    = 5184000 Sec   |
3 Months    = 7776000 Sec   |
4 Months    = 10368000 Sec  |
5 Months    = 12960000 Sec  |
6 Months    = 15552000 Sec  |
7 Months    = 18144000 Sec  |
8 Months    = 20736000 Sec  |
9 Months    = 23328000 Sec  |
10 Months   = 25920000 Sec  |
11 Months   = 28512000 Sec  |
12 Months   = 31104000 Sec  |
----------------------------+
" ), false );
$cli->output( $cli->stylize( 'green', "\nDone !\n" ), false );
$cli->output( $cli->stylize( 'yellow', "See {$logg} file for more details.\n" ), false );
$cli->output( $cli->stylize( 'cyan', "Peak memory usage : " . number_format( memory_get_peak_usage(), 0, '.', ' ' ) . " octets\n\n" ), false );
$script->shutdown();
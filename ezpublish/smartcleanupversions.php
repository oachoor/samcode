#!/usr/bin/env php
<?php
/**
 * File containing the script to cleanup versions according content.ini/[VersionManagement]/DefaultVersionHistoryLimit
 * and VersionHistoryClass settings
 *
 * @copyright Copyright (C) 1999-2013 eZ Systems AS. All rights reserved.
 * @license http://ez.no/Resources/Software/Licenses/eZ-Business-Use-License-Agreement-eZ-BUL-Version-2.1 eZ Business Use License Agreement eZ BUL Version 2.1
 * @version 5.2.0
 * @package
 */

require_once 'autoload.php';

$first  = new DateTime();
$cli    = eZCLI::instance();
$script = eZScript::instance( array( 'description' => "Remove archived content object versions according to "
                                                    . "[VersionManagement/DefaultVersionHistoryLimit and "
                                                    . "[VersionManagement]/VersionHistoryClass settings",
                                     'use-session' => false,
                                     'use-modules' => true,
                                     'use-extensions' => true ) );
$script->startup();
$options = $script->getOptions( "[limit:][offset:][class-id:][status:][n]", "", array( "limit" => "Objects limit",
                                                                                       "offset" => "Set Offset",
                                                                                       "class-id" => "Set Classtype",
                                                                                       "status" => "Fetch specific status",
                                                                                       "n" => "Do not wait" ) );
$script->initialize();

if ( !isset( $options['n'] ) )
{
    $cli->warning( "This cleanup script is going to remove archived versions according to the settings" );
    $cli->warning( "content.ini/[VersionManagement]/DefaultVersionHistoryLimit and content.ini/[VersionManagement]/VersionHistoryClass" );
    $cli->warning();
    $cli->warning( "You have 5 seconds to break the script (press Ctrl-C)" );
    sleep( 5 );
    $cli->output();
}

$subTreeParams = array( 'Limitation' => array(),
                        'MainNodeOnly' => true,
                        'LoadDataMap' => false,
                        'IgnoreVisibility' => true );

if ( isset( $options['class-id'] ) )
{
    $filter = array( 'ClassFilterType' => 'include', 'ClassFilterArray' => array( $options['class-id'] ) );
    $subTreeParams = array_merge( $subTreeParams, $filter );
}

$total = eZContentObjectTreeNode::subTreeCountByNodeID( $subTreeParams, 1 );
$cli->output( $cli->stylize( 'gray', "\n{$total} objects to check... (In the progess bar, 'R' means that at least a version was removed)\n" ), false );

$incIteration               = 0;
$subTreeParams['Offset']    = 0;
$subTreeParams['Limit']     = 100;
$strStatus                  = 'archived';
$db                         = eZDB::instance();
$subTreeParams['SortBy']    = array( 'published', true );
$status                     = eZContentObjectVersion::STATUS_ARCHIVED;

if ( isset( $options['limit'] ) )
    $limit = $options['limit'];
else
{
    $limit = $total < 5000 ? $total : 5000;
    $cli->warning( "Missing limit argument, ". $limit ." objects will be processed." );
}

$script->setIterationData( 'R', '.' );
$script->resetIteration( $limit );

if ( isset( $options['offset'] ) )
    $subTreeParams['Offset'] = $options['offset'];
else
    $cli->warning( "Missing offset argument, Offset is now set to 0." );

if ( isset( $options['status'] ) )
{
    $constStatus    = 'STATUS_'. strtoupper( $options['status'] );
    $reflector      = new ReflectionClass( 'eZContentObjectVersion' );
    $constants      = $reflector->getConstants();

    if ( array_key_exists( $constStatus, $constants ) && in_array( constant( 'eZContentObjectVersion::'. $constStatus ), $constants ) )
    {
        $strStatus  = $options['status'];
        $status     = constant( 'eZContentObjectVersion::'. $constStatus );
    }

    unset( $reflector );
}

$cli->warning( "Cleanup {$strStatus} versions." );
$logFile = 'Cleanup_'. $strStatus .'_versions_'. date( 'd_m_Y_h_i_s' ) .'.log';

while ( true )
{
    $nodes = eZContentObjectTreeNode::subTreeByNodeID( $subTreeParams, 1 );

    if ( empty( $nodes ) )
        break;

    foreach( $nodes as $node )
    {
        $incIteration++;
        $object = $node->attribute( 'object' );
        $versionCount = $object->getVersionCount();
        $versionLimit = eZContentClass::versionHistoryLimit( $object->attribute( 'content_class' ) );

        if ( $versionCount <= $versionLimit )
        {
            eZLog::write( "Nothing to do on object #{$object->attribute( 'id' )}", $logFile );
            $script->iterate( $cli, false, "Nothing to do on object #{$object->attribute( 'id' )}" );
            continue;
        }

        $removedVersion     = 0;
        $versionToRemove    = $strStatus == 'published' ? 10 : ( $versionCount - $versionLimit );
        $versions           = $object->versions( true, array( 'conditions' => array( 'status' => $status ),
                                                              'sort' => array( 'modified' => 'asc' ),
                                                              'limit' => array( 'limit' => $versionToRemove, 'offset' => 0 ) ) );
        $versionsCount      = count( $versions );

        /**
         * Published version
         * Make sure that we keep the last published version and remove the rest !!!
         */
        if ( $strStatus == 'published' && $versionsCount > 0 )
            array_pop( $versions );

        if ( $versionsCount > 0 )
        {
            $db->begin();

            foreach( $versions as $version )
            {
                $version->removeThis();
                $removedVersion++;
            }

            $db->commit();
        }

        if ( $removedVersion > 0 )
        {
            eZLog::write( "Removed {$removedVersion} {$strStatus} versions of object #{$object->attribute( 'id' )}", $logFile );
            $script->iterate( $cli, true, "Removed {$removedVersion} {$strStatus} versions of object #{$object->attribute( 'id' )}" );
        }
        else
        {
            eZLog::write( "No {$strStatus} version of object #{$object->attribute( 'id' )} found", $logFile );
            $script->iterate( $cli, false, "No {$strStatus} version of object #{$object->attribute( 'id' )} found" );
        }
    }

    $subTreeParams['Offset'] += $subTreeParams['Limit'];
    eZContentObject::clearCache();

    if ( $incIteration == $limit )
        break;
}

$diff       = $first->diff( new DateTime() );
$elapsed    = $diff->format( '%H:%I:%S' );

$cli->output( $cli->stylize( 'green', "\nElapsed time : ". $elapsed ."\n" ), false );
eZLog::write( "Elapsed time : ". $elapsed, $logFile );

$cli->output( $cli->stylize( 'cyan', "Peak memory usage : " . number_format( memory_get_peak_usage(), 0, '.', ' ' ) . " octets\n\n" ), false );
$script->shutdown();
#!/usr/bin/env php
<?php

/**
 * Cleanu unwanted classes attributes
 *
 * @option -n : Do not wait to cancel "Optional"
 * @option --shut : Exit the wash after given period "Optional", default 4 hours.
 * @option --seq : Exit the wash after given period "Optional", default 5 hours.
 * @option --class ( class id OR class identifier ) "Required"
 * @option --attr ( class attributes ids : Attribute ID, Attribute Identifier OR Attribute Name ) "Required"
 *
 * Remove class attribute exp: ( --attr=450 OR --attr=title )
 * Remove multiple class attributes : separate ids with semicolon exp : ( --attr="450;title;52;description" )
 */
require 'autoload.php';

$First      = new DateTime();
$Cli        = eZCLI::instance();
$LogFile    = 'Cleanup_attributes_'. date( 'd_m_Y' ) .'.log';
$Script     = eZScript::instance( array( 'description' => ( "Clear Obsolete Classes Attributes... \n" ),
                                         'use-session' => false,
                                         'use-modules' => true,
                                         'use-extensions' => true ) );
$Script->startup();

$Options    = $Script->getOptions( '[class:][attr:][shut:][seq:][n]', '', array( 'class' => 'ID of class to clear',
                                   'attr' => 'ID of attribute to clear (Delimiter ids with ";")',
                                   'shut' => 'Shutting down the script at a given time.',
                                   'seq' => 'Sequential cleaning, attribute by attribute',
                                   'n' => 'Do not wait' ) );
$Script->initialize();

if ( !isset( $Options['n'] ) )
{
    $Cli->warning( "\nThis cleanup script is going to remove uwanted attributes according to the parameters\n" );
    $Cli->warning( "You have 5 seconds to break the script (press Ctrl-C)\n" );
    sleep( 5 );
    $Cli->output();
}

if ( isset( $Options['class'] ) && $Options['class'] != '' )
{
    $ClassID    = $Options['class'];
    $Class      = is_numeric( $ClassID ) ? eZContentClass::fetch( $ClassID ) : eZContentClass::fetchByIdentifier( $ClassID );

    if ( $Class instanceof eZContentClass )
    {
        $AttrSkipped    = 0;
        $AttrRejected   = array();
        $ObjetCount     = $Class->objectCount();
        $Attributes     = $Class->fetchAttributes();

        /**
         * Elapsed time :
         * 1 hour : 3600
         * 2 hours : 7200
         * 3 hours : 10800
         * 4 hours : 14400 (seq : default)
         * 5 hours : 18000 (shut : default)
         */
        $SeqTime    = isset( $Options['seq'] ) ? $Options['seq'] : 3600;
        $ShutTime   = isset( $Options['shut'] ) ? $Options['shut'] : 10800;

        $Cli->output( "Fetch ". $Class->attribute( 'name' ) ." class attributes and {$ObjetCount} objects...\n" );
        $Cli->output( "This script is going to stop maybe after {$SeqTime} seconds, but certainly in {$ShutTime} seconds.\n" );

        if ( isset( $Options['attr'] ) )
        {
            if ( strpos( $Options['attr'], ';' ) !== false )
                $AttrRejected = array_map( 'trim',explode( ';', $Options['attr'] ) );
            else
                array_push( $AttrRejected, trim( $Options['attr'] ) );
        }

        if ( count( $Attributes ) > 0 && count( $AttrRejected ) > 0 )
        {
            foreach ( $Attributes as $Attribute )
            {
                if ( in_array( $Attribute->ID, $AttrRejected ) || in_array( $Attribute->Identifier, $AttrRejected ) )
                {
                    /**
                     * Is there existing objects of this content class attribute ?
                     * Delete request class attribute from all objects, this may take a little while...
                     */
                    $DeletedCount   = 0;
                    $hasFinished    = false;

                    $Cli->output( "Fetch all objects for '". $Attribute->attribute( 'name' ) ."' attribute, This may take a little while...\n" );

                    while ( true )
                    {
                        $ObjectAttributes = eZContentObjectAttribute::fetchSameClassAttributeIDList( $Attribute->ID, true, false, false, array( 'offset' => 0, 'limit' => 100 ) );

                        if ( empty( $ObjectAttributes ) || count( $ObjectAttributes ) == 0 )
                        {
                            $hasFinished = true;
                            break;
                        }

                        foreach ( $ObjectAttributes as $Attr )
                        {
                            if ( $Attr->removeThis( $Attr->attribute( 'id' ) ) !== false )
                            {
                                $DeletedCount++;
                                eZLog::write( "Attribute #{$Attribute->ID} removed from #{$Attr->ContentObjectID}", $LogFile );
                            }
                        }

                        /**
                         * Check for elapsed time
                         */
                        if ( ( time() - $First->getTimestamp() ) >= $ShutTime )
                        {
                            eZLog::write( "The script was interrupted suddenly at ". date( 'H:i:s' ) ."\n", $LogFile );
                            eZLog::write( $Attribute->attribute( 'name' ) ." : Attribute has been removed from ". $DeletedCount . " objects.\n", $LogFile );
                            $Cli->warning( $Attribute->attribute( 'name' ) ." : Attribute has been removed from ". $DeletedCount . " objects.\n" );
                            break 2; // Exit from while and foreach loop
                        }

                        eZContentObject::clearCache();
                    }

                    /**
                     * Check if cleaning objects has finished, if true, so remove attribute from the class !
                     */
                    if ( $hasFinished === true )
                    {
                        if ( $Attribute->removeThis() !== false )
                        {
                            eZLog::write( $Attribute->attribute( 'name' ) ." : Attribute has been removed from '". $Class->attribute( 'name' ) ."' class. \n", $LogFile );
                            $Cli->warning( $Attribute->attribute( 'name' ) ." : Attribute has been removed from '". $Class->attribute( 'name' ) ."' class. \n" );
                        }
                        else
                        {
                            $RemoveInfo = $Attribute->dataType()->classAttributeRemovableInformation( $Attribute );

                            if ( $RemoveInfo !== false )
                            {
                                eZLog::write( "Error while cleaning ". $Attribute->attribute( 'name' ) ." class attribute : ". $RemoveInfo ."\n", $LogFile );
                                $Cli->error( "Error while cleaning ". $Attribute->attribute( 'name' ) ." class attribute : ". $RemoveInfo ."\n" );
                            }
                        }

                        eZLog::write( $Attribute->attribute( 'name' ) . " : Attribute has been removed from " . $DeletedCount . " objects.\n", $LogFile );
                        $Cli->warning( $Attribute->attribute( 'name' ) . " : Attribute has been removed from " . $DeletedCount . " objects.\n" );

                        /**
                         * Check for elapsed time
                         */
                        if ( ( time() - $First->getTimestamp() ) >= $SeqTime )
                        {
                            eZLog::write( "The script was interrupted normally at ". date( 'H:i:s' ) ."\n", $LogFile );
                            break 1; // Exit from foreach loop
                        }
                    }
                }
                else
                    $AttrSkipped++;
            }

            if ( ( count( $Attributes ) - $AttrSkipped ) == 0 )
            {
                eZLog::write( "Already cleaned : Check to determine if the attribute exists in '". $Class->attribute( 'name' ) ."' class. \n", $LogFile );
                $Cli->error( "Already cleaned : Check to determine if the attribute exists in '". $Class->attribute( 'name' ) ."' class. \n" );
            }
            else
                $Cli->output( $Cli->stylize( 'cyan', "\nDone :-)\n" ), false );
        }
        else
        {
            eZLog::write( "Class Attribute list is empty !, No attribute will be deleted. \n", $LogFile );
            $Cli->error( "Class Attribute list is empty !, No attribute will be deleted. \n" );
        }
    }
    else
    {
        eZLog::write( "No class with ID : {$ClassID} \n", $LogFile );
        $Cli->error( "No class with ID : {$ClassID} \n" );
    }
}

$Diff = $First->diff( new DateTime() );
eZLog::write( "Elapsed time : ". $Diff->format( '%H:%I:%S' ) ."\n", $LogFile );

$Cli->output( $Cli->stylize( 'green', "\nElapsed time : ". $Diff->format( '%H:%I:%S' ) ."\n" ), false );
$Cli->output( $Cli->stylize( 'cyan', "Peak memory usage : " . number_format( memory_get_peak_usage(), 0, '.', ' ' ) . " octets\n\n" ), false );

$Script->shutdown();